package com.uz.secondsuser.util

import java.util.regex.Pattern

class Utilities {


    companion object {
        fun isEmailValid(email: String?): Boolean {
            val emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                    "[a-zA-Z0-9_+&*-]+)*@" +
                    "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                    "A-Z]{2,7}$"
            val pat = Pattern.compile(emailRegex)
            return if (email == null) false else pat.matcher(email).matches()
        }



        fun isSpace(character: String):Boolean{

            val trimmed = character.trim()
            val isSpace = trimmed.isEmpty()

            if(isSpace){
                return true
            }
            return false

        }

    }

}