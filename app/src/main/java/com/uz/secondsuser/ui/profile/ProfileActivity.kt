package com.uz.secondsuser.ui.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import com.uz.secondsuser.Injection
import com.uz.secondsuser.R
import com.uz.secondsuser.repo.ProfilePresenter
import com.uz.secondsuser.repo.ProfileRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {


    var profileViewModel: ProfileViewModel ? = null
    var profilePresenter : ProfilePresenter? = null
    var userRepo : ProfileRepository? = null

    var userId : String ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        setUpToolBar()
        setUpComponents()

        val b = intent.extras
        if(b!=null){
            userId = b.getString("userId")
            Log.d("ProfilePresenter-UserId","user: "+userId)
            userRepo?.getUser(userId!!)
        }

        displayProfile()


    }

    private fun setUpToolBar() {

        setSupportActionBar(toolbar_profile)

           supportActionBar!!.setDisplayHomeAsUpEnabled(true)
           //supportActionBar!!.setIcon(R.drawable.ic_logo)
           supportActionBar!!.setDisplayShowTitleEnabled(true)

        val actionbar = supportActionBar

        actionbar?.title = "Profile"


        collapsing_toolbar_profile.isTitleEnabled = false


        toolbar_profile.setNavigationOnClickListener {
            finish()
        }


    }

    private fun setUpComponents(){
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        profilePresenter = Injection.provideProfilePresenter(profileViewModel!!)
        userRepo = Injection.provideProfileRepo(profilePresenter!!,Injection.provideUserApi())

    }



    private fun displayProfile(){

        profileViewModel?.usernameLiveData!!.observe(this, Observer {

             tv_username.text = it

        })
        profileViewModel?.emailLiveData!!.observe(this, Observer {

             tv_email.text = it

        })
        profileViewModel?.genderLiveData!!.observe(this, Observer {

             tv_gender.text = it

        })
        profileViewModel?.occupationLiveData!!.observe(this, Observer {

             tv_user_occupation.text = it
        })


        profileViewModel?.profileUriLiveData!!.observe(this, Observer {

            // val imageUri = "https://i.imgur.com/tGbaZCY.jpg"
            //.placeholder(R.drawable.ic_movie)
            val imageUri = it
            Picasso.with(iv_profile.context).load(imageUri)
                .fit().centerCrop()
                .into(iv_profile)
        })




    }




}
