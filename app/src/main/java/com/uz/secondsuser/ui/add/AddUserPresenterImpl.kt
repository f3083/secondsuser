package com.uz.secondsuser.ui.add

import com.uz.secondsuser.repo.AddUserPresenter
import com.uz.secondsuser.util.Utilities

class AddUserPresenterImpl(private val addUserViewModel: AddUserViewModel) : AddUserPresenter {



    //presents errors
   override fun checkName(userName: String){
        var error = "InvalidName"
        if (userName.length in 16 downTo 0 && Utilities.isSpace(userName)) {
            addUserViewModel.mUsernameError.postValue(error)
        }

    }

   override fun checkEmail(email: String){
        var e1 = "InvalidEmail"
        var e2 = "Remove Spaces"
        if (!Utilities.isEmailValid(email)) {
            if(Utilities.isSpace(email)){
                addUserViewModel.mEmailError.postValue(e2)
            }else{
                addUserViewModel.mEmailError.postValue(e1)
            }

        }
    }

    override fun checkOccupation(occupation: String){
        var error = "Invalid Occupation"
        if (occupation.length in 16 downTo 0 && Utilities.isSpace(occupation)) {
            addUserViewModel.mOccupationError.postValue(error)
        }
    }

    override fun isDataValid(userName: String, email: String, occupation: String) {

        if(!Utilities.isSpace(occupation) && !Utilities.isSpace(userName) && Utilities.isEmailValid(email)){
           addUserViewModel.mIsDataValid.postValue(true)
        }else{
            addUserViewModel.mIsDataValid.postValue(false)
        }

    }

    override fun isShowSuccess(isSuccess: Boolean,recordingMsg: String){
        addUserViewModel.mIsSuccess.postValue(isSuccess)
        if(isSuccess){
            addUserViewModel.mRecordUserMsg.postValue(recordingMsg)
        }else{
             addUserViewModel.mFailMsg.postValue(recordingMsg)
        }

    }




}