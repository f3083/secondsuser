package com.uz.secondsuser.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileViewModel : ViewModel() {

    val mUsername = MutableLiveData<String>()
    val usernameLiveData : LiveData<String> = mUsername

    val mEmail = MutableLiveData<String>()
    val emailLiveData : LiveData<String> = mEmail

    val mOccupation= MutableLiveData<String>()
    val occupationLiveData : LiveData<String> = mOccupation

    val mGender= MutableLiveData<String>()
    val genderLiveData : LiveData<String> = mGender

    val mProfileUri = MutableLiveData<String>()
    val profileUriLiveData : LiveData<String> = mProfileUri


    val mError = MutableLiveData<String>()
    val errorLiveData : LiveData<String> = mError


}