package com.uz.secondsuser.ui.profile

import android.provider.ContactsContract
import android.util.Log
import com.uz.secondsuser.model.SecondsUser
import com.uz.secondsuser.repo.ProfilePresenter
import com.uz.secondsuser.util.Utilities

class ProfilePresenterImpl(private val profileViewModel: ProfileViewModel) : ProfilePresenter {

    override fun showProfile(user: SecondsUser) {

        Log.d("ProfilePresenter","user: "+user.userName)

        profileViewModel.mUsername.postValue(user.userName)
        profileViewModel.mEmail.postValue(user.userEmail)
        profileViewModel.mGender.postValue(user.userGender)
        profileViewModel.mOccupation.postValue(user.userOccupation)
        profileViewModel.mProfileUri.postValue(user.userProfileUrl)

    }

    override fun showError(msg: String) {
        profileViewModel.mError.postValue(msg)
    }
    
}