package com.uz.secondsuser.ui.add

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.FirebaseApp
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.uz.secondsuser.Injection
import com.uz.secondsuser.R
import com.uz.secondsuser.model.SecondsUser
import com.uz.secondsuser.repo.AddUserRepository
import com.uz.secondsuser.ui.profile.ProfileActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {


    var addUserViewModel : AddUserViewModel ? = null
    var inputPresenter : AddUserPresenterImpl ? = null
    var userRepo : AddUserRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        FirebaseApp.initializeApp(this)
        var  storageRef : StorageReference = FirebaseStorage.getInstance().reference


         setUpComponents(storageRef)
        selectEditText()


        requestPermission()
        iv_user.setOnClickListener {
            openGallery()
        }


        saveUser()
        observeUI()

    }

    private fun setUpComponents(reference: StorageReference){
        addUserViewModel = ViewModelProviders.of(this).get(AddUserViewModel::class.java)
        inputPresenter = Injection.provideInputPresenter(addUserViewModel!!)
        userRepo = Injection.provideAddUserRepo(inputPresenter!!,Injection.provideUserApi(),reference)
    }

    private var userName :String ? = null
    private var userEmail :String ? = null
    private var userOccupation :String ? = null
    private var userProfileUrl :String?  = null
    private var userGender :String ? = null




    //////////////  PERMISSIONS  ///////////////////////////////////////
    private var MY_PERMISSIONS_REQUEST_CODE = 25


    private fun requestPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_CODE)
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }


    private fun isPermissionGranted(permission:String):Boolean = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    override fun onRequestPermissionsResult(requestCode: Int, thePermissions: Array<String>, theGrantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, thePermissions, theGrantResults)
        // It's not our expect permission
        if (requestCode != MY_PERMISSIONS_REQUEST_CODE) return

        var per = Manifest.permission.READ_EXTERNAL_STORAGE

        if (isPermissionGranted(per)) {
            // Success case: Get the permission
            Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show()
            // Do something and return
            return
        }

        //  if (isUserCheckNeverAskAgain()) {
        // NeverAskAgain case - Never Ask Again has been checked
        // Do something and return
        return
        // }

        // Failure case: Not getting permission
        // Do something here
    }

    ///////////////////////////////////////////////////////////////////






    //1. GET IMAGE



    private var profileImagePath : Uri? = null
    private fun openGallery() {
        //val intent = Intent()
        //intent.type = "video/*"
        //intent.action = Intent.ACTION_GET_CONTENT
        // startActivityForResult(Intent.createChooser(intent, "Select Video"),REQUEST_CODE)

            if (Build.VERSION.SDK_INT < 20) {
                // var photoPickerIntent = Intent(Intent.ACTION_PICK);
                // photoPickerIntent.type = "video/*"
                // photoPickerIntent.setType("image/* video/*");
                // val  mimetypes = arrayOf("image/*", "video/*")
                // photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                // startActivityForResult(photoPickerIntent, REQUEST_CODE)
            } else {

                //Intent.ACTION_OPEN_DOCUMENT and NOT Intent.ACTION_GET_DOCUMENT whereas later one is like a one-time thing.
                var photoPickerIntent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                photoPickerIntent.addCategory(Intent.CATEGORY_OPENABLE)
                photoPickerIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
                photoPickerIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                photoPickerIntent.type = "image/*";
                startActivityForResult(photoPickerIntent, REQUEST_CODE_IMG)
            }


    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMG){

            if (data?.data != null) {
                profileImagePath = data.data!!

                try {
                    var bitmap = MediaStore.Images.Media.getBitmap(contentResolver, profileImagePath)
                        iv_user.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace();
                }
            }

        }else{
            //solves error when vid not selected
            finish()
        }
    }






    //2. GET TEXT FROM RADIO BUTTON
    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.radioGenderMale ->
                    if (checked) {
                        // Pirates are the best
                        userGender = view.text.toString()
                    }
                R.id.radioGenderFemale ->
                    if (checked) {
                        // Ninjas rule
                        userGender = view.text.toString()
                    }
            }
        }
    }



    //3. GET TEXT FROM EDIT TEXT
    private fun selectEditText(){

      //  val radioGroup = radioGenderGroup
       // val checkedButton = findViewById<RadioButton>(radioGroup.checkedRadioButtonId)

        editTextUsername.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                }

                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                        inputPresenter?.checkName(charSequence.toString())

                }
                override fun afterTextChanged(editable: Editable) {

                   // userName = editTextUsername.text.toString()

                    // editable.clear();
                }
            })

        editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                    inputPresenter?.checkEmail(charSequence.toString())

            }
            override fun afterTextChanged(editable: Editable) {

                // editable.clear();
            }
        })


        editTextOccupation.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                inputPresenter?.checkOccupation(charSequence.toString())

            }
            override fun afterTextChanged(editable: Editable) {

                // editable.clear();
            }
        })



       // userGender = checkedButton.text.toString()

    }





    private fun observeUI(){

        addUserViewModel?.usernameErrorLiveData!!.observe(this, Observer {

            editTextUsername.error = it
        })

        addUserViewModel?.emailErrorLiveData!!.observe(this, Observer {

            editTextEmail.error = it
        })

        addUserViewModel?.occupationErrorLiveData!!.observe(this, Observer {

           editTextOccupation.error = it
        })

        addUserViewModel?.isSuccessLiveData!!.observe(this, Observer {isSuccess ->

            progressBarSaveInfo.visibility = View.GONE
            if(isSuccess) {
                addUserViewModel?.recordUserMsgLiveData!!.observe(this, Observer { userId->

                    //Toast.makeText(this, "isSuccess: $userId",Toast.LENGTH_LONG).show()
                    val intent = Intent(this, ProfileActivity::class.java)
                    val b = Bundle()
                    b.putString("userId", userId)
                    intent.putExtras(b)
                    startActivity(intent)

                })
            }else{
                progressBarSaveInfo.visibility = View.GONE
                addUserViewModel?.failLiveData!!.observe(this, Observer {

                    progressBarSaveInfo.visibility = View.GONE
                    Toast.makeText(this, it,Toast.LENGTH_LONG).show()

                })

            }

        })




    }



    private fun saveUser(){


        buttonSaveUser.setOnClickListener {

            userName = editTextUsername.text.toString()
            userEmail = editTextEmail.text.toString()
            userOccupation = editTextOccupation.text.toString()

            progressBarSaveInfo.visibility = View.VISIBLE


            inputPresenter?.isDataValid(userName!!,userEmail!!,userOccupation!!)

            addUserViewModel?.isDataValidLiveData!!.observe(this, Observer { isDataValid ->


                if(isDataValid){

                    userRepo?.saveUser(userName!!,userEmail!!,userOccupation!!,userGender!!,profileImagePath!!)

                }else{

                    Toast.makeText(this, "Please fill in all values!", Toast.LENGTH_SHORT).show()
                    progressBarSaveInfo.visibility = View.GONE

                }

            })


        }

        buttonTest.setOnClickListener {

            val intent = Intent(this,ProfileActivity::class.java)
            val b = Bundle()
            b.putString("userId","1234")
            intent.putExtras(b)
            startActivity(intent)
        }



    }




    companion object{

        private const val REQUEST_CODE_IMG = 1
        lateinit var storageReference : StorageReference

    }




}
