package com.uz.secondsuser.ui.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AddUserViewModel : ViewModel() {

    val mUsernameError = MutableLiveData<String>()
    val usernameErrorLiveData : LiveData<String> = mUsernameError

    val mEmailError = MutableLiveData<String>()
    val emailErrorLiveData : LiveData<String> = mEmailError

    val mOccupationError = MutableLiveData<String>()
    val occupationErrorLiveData : LiveData<String> = mOccupationError


    val mIsDataValid = MutableLiveData<Boolean>()
    val isDataValidLiveData : LiveData<Boolean> = mIsDataValid



    val mIsSuccess = MutableLiveData<Boolean>()
    val isSuccessLiveData : LiveData<Boolean> = mIsSuccess

    val mRecordUserMsg = MutableLiveData<String>()
    val recordUserMsgLiveData : LiveData<String> = mRecordUserMsg

    val mFailMsg = MutableLiveData<String>()
    val failLiveData : LiveData<String> = mFailMsg




}