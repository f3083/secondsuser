package com.uz.secondsuser.model

data class SecondsUser(val id:String,
                       val userName:String,
                       val userEmail:String,
                       val userGender:String,
                       val userOccupation:String,
                       val userProfileUrl:String)