package com.uz.secondsuser

import com.google.firebase.storage.StorageReference
import com.uz.secondsuser.repo.*
import com.uz.secondsuser.ui.profile.ProfilePresenterImpl
import com.uz.secondsuser.ui.profile.ProfileViewModel
import com.uz.secondsuser.ui.add.AddUserPresenterImpl
import com.uz.secondsuser.ui.add.AddUserViewModel

class Injection {


    companion object {

      fun provideInputPresenter(addUserViewModel: AddUserViewModel): AddUserPresenterImpl {

        return AddUserPresenterImpl(addUserViewModel)
       }


        fun provideProfilePresenter(profileViewModel:  ProfileViewModel):ProfilePresenter {

            return ProfilePresenterImpl(profileViewModel)
        }

      fun provideAddUserRepo(addUserPresenter: AddUserPresenter, userApi:UserApi, storageReference: StorageReference):AddUserRepository {

       return AddUserRepository(addUserPresenter,userApi,storageReference)

       }

        fun provideProfileRepo(profilePresenter: ProfilePresenter, userApi:UserApi):ProfileRepository{

            return ProfileRepository(profilePresenter,userApi)

        }

       fun provideUserApi():UserApi {

            var api = APIClient().getRetrofitInstance()!!.create(UserApi::class.java)
           return api

       }






    }



}