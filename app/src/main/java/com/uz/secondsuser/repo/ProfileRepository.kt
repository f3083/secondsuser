package com.uz.secondsuser.repo

import android.net.Ikev2VpnProfile
import android.net.Uri
import android.util.Log
import com.google.firebase.storage.StorageReference
import com.uz.secondsuser.model.SecondsUser
import com.uz.secondsuser.repo.AddUserPresenter
import com.uz.secondsuser.repo.ProfilePresenter
import com.uz.secondsuser.repo.UserApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.*

class ProfileRepository(private val profilePresenter: ProfilePresenter, private val api:UserApi) {

    fun getUser(userId:String){

        api.getUser(userId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : DisposableObserver<SecondsUser>() {//PublishResponse

                override fun onNext(user: SecondsUser) {

                    Log.d("ProfilePresenter-Repo","user: "+user.userName)


                    profilePresenter.showProfile(user)

                }

                override fun onError(e: Throwable) {
                    val error = "Error: "+e.message
                    Log.d("ProfilePresenter-error","e: "+e.message)


                    profilePresenter.showError(error)


                }

                override fun onComplete() {


                }

            })


    }




}