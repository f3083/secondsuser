package com.uz.secondsuser.repo

import android.provider.ContactsContract
import com.uz.secondsuser.model.SecondsUser
import com.uz.secondsuser.util.Utilities

interface ProfilePresenter{

    //presents errors
    fun showProfile(user: SecondsUser)
    fun showError(msg:String)


}