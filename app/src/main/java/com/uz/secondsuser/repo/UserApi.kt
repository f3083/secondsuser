package com.uz.secondsuser.repo

import com.uz.secondsuser.model.SecondsUser
import io.reactivex.Observable
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface UserApi {

    @POST("users/addUser/")
    fun saveUser(@Body user: SecondsUser): Observable<SecondsUser>

    @GET("users/getUser/{userId}")
    fun getUser(@Path("userId") userId:String) : Observable<SecondsUser>


}