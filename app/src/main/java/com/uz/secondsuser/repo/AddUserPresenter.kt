package com.uz.secondsuser.repo

import android.provider.ContactsContract
import com.uz.secondsuser.util.Utilities

interface AddUserPresenter{

    //presents errors
    fun checkName(userName: String)

    fun checkEmail(email: String)

    fun checkOccupation(occupation: String)

    fun isDataValid(userName: String,email: String,occupation: String)

    fun isShowSuccess(isSuccess:Boolean,recordingMsg: String)


}