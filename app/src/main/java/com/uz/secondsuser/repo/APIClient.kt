package com.uz.secondsuser.repo

import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


class APIClient {





    private val ENDPOINT_FIREBASE = "https://us-central1-seconds-d6bce.cloudfunctions.net/api/"
    private val ENDPOINT_HEROKU = "https://rocky-coast-55358.herokuapp.com/api/"

    //  private val ENDPOINT = "http://10.0.2.2:8080/api/"
    private val ENDPOINT = "http://10.42.0.1:8080/api/"

    private var retrofit: Retrofit? = null

    fun getRetrofitInstance(): Retrofit? {
        if (retrofit == null) { //to fake responses

            val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(10000, TimeUnit.SECONDS) //40 //100
                .readTimeout(200, TimeUnit.SECONDS) //60 //200
                .writeTimeout(200, TimeUnit.SECONDS) //90 //200


                    /*
                .addInterceptor(object : Interceptor { @Throws(IOException::class)
                    override fun intercept(chain: Interceptor.Chain): Response {


                    return chain.proceed(chain.request())
                        .newBuilder()
                        .code(200)
                        .protocol(Protocol.HTTP_2)
                        .message(getJsonUser)
                        .body(ResponseBody.create("application/json".toMediaTypeOrNull(), getJsonUser.toByteArray()))
                        .addHeader("content-type", "application/json")
                        .build()
                    }
                     })
                    */



                .build()
            val gson = GsonBuilder()
                .setLenient()
                .create()

            retrofit = Retrofit.Builder()
                .baseUrl(ENDPOINT_HEROKU)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }
        return retrofit
    }



   var getListJson = """
         {
	"channelId": 1234,
	"channelName": "Hello-World",
	"channelDescription": "Describe",
	"channelAvatorUrl": "Url",
	"videos":[],
	"subscriptions":[]
        }
     """

    var getJsonUser = """
         {
	"userId": 1234,
	"userName": "Peter",
	"userEmail": "wcpetr01@gmail.com",
	"userOccupation": "Doctor",
    "userGender": "Male",
    "userProfileUrl": ""
        }
     """

}