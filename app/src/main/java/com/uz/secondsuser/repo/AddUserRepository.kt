package com.uz.secondsuser.repo

import android.net.Uri
import android.util.Log
import com.google.firebase.storage.StorageReference
import com.uz.secondsuser.model.SecondsUser
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.util.*

class AddUserRepository(private val addUserPresenter: AddUserPresenter, private val api:UserApi,
                        private val  storageReference: StorageReference) {




    fun saveUser(userName:String,userEmail:String,userOccupation:String,userGender:String,profileImagePath: Uri){

        //publish assets first then get their url and save the data
        // Code for showing progressDialog while uploading
       // var progressDialog : ProgressDialog = ProgressDialog(this)
       // progressDialog.setTitle("Uploading Thumbnail...")
        // progressDialog.show()

        // Defining the child of storageReference
        val ref: StorageReference = storageReference.child("images/"+ UUID.randomUUID().toString())


        // Upload from a local file
        // adding listeners on upload
        // or failure of image
        val uploadTask =  ref.putFile(profileImagePath)
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads

            // Error, Image not uploaded
            //progressDialog.dismiss();
            //Toast.makeText(this, "Failed " + it.message, Toast.LENGTH_SHORT).show()

        }.addOnSuccessListener { taskSnapshot ->
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.

            // Image uploaded successfully
            // Dismiss dialog
            // progressDialog.dismiss();
            //Toast.makeText(this, "Image Uploaded!!"+taskSnapshot.uploadSessionUri, Toast.LENGTH_SHORT).show()

               ref.downloadUrl.addOnSuccessListener { downloadUri ->

                   val user = SecondsUser(UUID.randomUUID().toString(),userName,userEmail,userOccupation,userGender,downloadUri.toString())
                   recordUser(user)


            }.addOnFailureListener {
                // Handle any errors
            }

        }.addOnProgressListener {taskSnapshot ->
            // var  progress = (100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount)
            //  progressDialog.setMessage("Uploaded $progress%")
        }




    }

   private fun recordUser(user:SecondsUser){

        api.saveUser(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : DisposableObserver<SecondsUser>() {//PublishResponse

                override fun onNext(user: SecondsUser) {

                    //val userMsg = "Welcome"+user.userName+" "+user.userEmail
                    addUserPresenter.isShowSuccess(true,user.id)

                }

                override fun onError(e: Throwable) {
                    val userMsg = e.message?.split(" ")
                    val us1 = userMsg!![1]

                    var errorMsg = us1
                    if(us1 == "409"){
                        errorMsg = "Username ${user.userName} taken"
                    }

                   addUserPresenter.isShowSuccess(false,errorMsg)

                }

                override fun onComplete() {


                }

            })




    }





}